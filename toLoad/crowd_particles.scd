/**************************************************************

Project: VR_holoWEEN
File: crowd_particles.scd

Author: mmusick
Email: michael@michaelmusick.com

Created: 2015-10-16 19:57:18
Modified: 2015-10-17 11:41:03


Notes:

Server.killAll
o = Server.default.options;
o.sampleRate = 44100;
o.inDevice = "Built-in Output"; o.outDevice = "Built-in Output";
s.reboot


**************************************************************/


"Loading Crowd Particles".postln;

(

SynthDef(\person_cheering, {
	arg out = 0, bufnum, time = 50, side = -1, pitchShift = 1, timeShift = 1,
	distance = 0, startPos = 30.degrad, endPos = 150.degrad,
	dopplerRatio = 0.4, 	// ratio between [coming]:going
	dopplerDepth = 5,		// semtitones of pitch change
	synthId = 1, charNum = 0;
	var sig, env, trigger, t_gate, bufLen, pbRate, angle, azim, lowPass;

	// convert to linear amp value
	distance = ((-1) * distance).dbamp;
	// change side to opposite (this allows for a -1=left 1=right vibe)
	side = side.neg;

	// get the buffer length
	bufLen = BufDur.kr(bufnum);
	// randomly trigger the sample
	trigger = Dust.kr( (1/bufLen) );
	// gate the trigger so it cannot fire before it is finished
	trigger = TDelay.kr(trigger, bufLen);
	// change the bufnum
	SendReply.kr(trigger, '/ghost', [charNum, bufnum], synthId );
	trigger = TDelay.kr(trigger, 0.01);

	env = Env.new(
		[0, 1, 0],
		[dopplerRatio, (1-dopplerRatio)],
		[-6, -1]
	);

	env = EnvGen.kr(env, timeScale: time, doneAction: 2);



	// set the depth of pitch bend
	dopplerDepth = ( ((0.5).ampdb/12) * dopplerDepth ).dbamp;
	pbRate = env.linlin(0,1,dopplerDepth,1);
	pbRate = BufRateScale.kr(bufnum) * pbRate * timeShift;

	sig = PlayBuf.ar(1, bufnum, rate: pbRate, trigger: trigger, loop: 0);

	// // pitchShift
	// sig = PitchShift.ar(sig,
	// 	windowSize: 0.1,
	// 	pitchRatio: (pitchShift),
	// 	timeDispersion: 0.02
	// );

	// distance effects
	sig = sig * env * distance * 0.7;
	lowPass = env.linlin(0,1,1000, 22000);
	sig = LPF.ar( sig, lowPass );


	// ambisonic encoding
	sig = FoaEncode.ar(sig, ~aud.encoder);

	// transform the position of the signal
	angle = (pi/2) * env.linlin(0,1,1,0.6);
	azim = Line.kr( startPos, endPos, dur: time) * side;
	sig = FoaTransform.ar(sig, 'push', angle, azim);

	// ambisonic decoder - TEMPORARY
	// TODO: take this out of the code and make it a SynthDef

	// sig = ~renderDecode.value(sig, ~decoder);

	Out.ar(out, sig);

}).add;



);

(

// out = 0, bufnum, time = 50, side = -1,
// distance = 0, startPos = 30.degrad, endPos = 150.degrad,
// dopplerRatio = 0.4, 	// ratio between [coming]:going
// dopplerDepth = 3,		// semtitones of pitch change
// synthId = 1, charNum = 0;

// ~aud.task.stop;
~aud.task = Task({

	var synthIdNum, ptr, charArr, maxNum = 7000;

	ptr = 0;
	synthIdNum = 0;

	~aud.oscArray = Array.newClear(maxNum);

	inf.do({

		var buffArr, synth, side, charNum = 0, synthId = 1, waitTime=1, time;
		var distance, charBuffArr, charBuffArrSize;
		var pitchShift, timeShift;
		var bufnum, oscSym;

		if( s.avgCPU < 50, {

			if(ptr==0, {charArr = (0..(~aud.numChars-1)).scramble; charArr;});
			charNum = charArr[ptr];
			// "\nnew char num: ".post; charNum.postln;
			// synthIdNum.postln;

			charBuffArr = ~aud.charArr[charNum];
			charBuffArrSize = charBuffArr.size;
			bufnum = charBuffArr[charBuffArrSize.rand].bufnum;

			side = [-1,1].choose;
			time = 35 + rrand(-10, 30);
			waitTime = 3.0.rand.clip(0.01, 6);
			distance = rrand(2, 10);

			pitchShift = rrand(0.9, 1.2);
			timeShift =  rrand(0.8, 1.3);


			synth = Synth(\person_cheering, [
				\bufnum, bufnum, \time, time,
				\pitchShift, pitchShift, \timeShift, timeShift,
				\synthId, synthIdNum, \side, side, \charNum, charNum,
				\out, ~aud.out.index
			]);


			// if(~aud.oscArray[synthIdNum].notNil, {
			// 	~aud.oscArray[synthIdNum].free;
			// });



			synthIdNum = (synthIdNum + 1) % 7000;
			ptr = (ptr + 1) % ~aud.numChars;

		});
		waitTime.wait;
	});
}).start;




OSCFunc({
	|msg, time, addr, recvPort|
	var charNum, bufNum, charSize, nodeID;

	// msg.postln;
	// time.postln;
	// addr.postln;
	// recvPort.postln;

	nodeID = msg[1];
	charNum = msg[3];
	charSize = ~aud.charArr[charNum].size;
	bufNum = ~aud.charArr[charNum][charSize.rand].bufnum;
	// "".postln;

	Node.basicNew(s,nodeID).set(\bufnum, bufNum);



}, \ghost);

);

"DONE - Crowd Particles".postln;